 const gulp = require('gulp');
const sass = require('gulp-sass');

gulp.task('sass', function() {
	return gulp.src('./sass/*.scss')
	.pipe(sass())
	.pipe(gulp.dest('./public'));
})

gulp.task('watch', function(){
	gulp.watch('./sass/*scss', ['sass']);
})
gulp.task('default', ['sass', 'watch']);