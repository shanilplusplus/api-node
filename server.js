const express =  require('express');
const bodyParser =  require('body-parser');
const app = express();
const request = require('request');
const port = process.env.PORT || 8008;
app.use(express.static(__dirname + '/public'));
app.use('/', express.static(__dirname + '/www'));
app.use('/js', express.static(__dirname + '/node_modules/bootstrap/dist/js'));
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));
app.use('/js', express.static(__dirname + '/node_modules/jquery/dist'));
app.use(bodyParser.urlencoded({extended: true}));
app.set('view engine', 'ejs');


app.get('/', function(req, res){
	res.render('home.ejs');	
});
app.get('/about', function(req, res){
	res.render('about');	
});
app.get('/contact', function(req, res){
	res.render('contact', {page : 'contact '});
});

app.post('/', function(req, res){
	// var search = req.query.search;
	var searchQuery = req.body.search;
	// console.log(req.body.mess);
	// console.log(searchQuery);
	var url = "http://food2fork.com/api/search?key=4b7287373e433cb6ed1c7b5781a42ed2&q=" + searchQuery;
	// console.log(url["recipes"]["image_url"]);

	request(url, function(error, response,body){
		if(!error && response.statusCode == 200){
			var data = JSON.parse(body);
			var id = data['recipes'];
			// console.log(id);
			res.render("home", {
				data: data,
				id: id
			});
		}
	});
});


app.post('/contact', function(req, res){
	// console.log(req.body);
	var name = req.body.name;
	res.render('contact', {
		page : 'contact ',
		name: name
	});	
}); 
app.get('/recipe/:recipe', function(req, res){
	var id_url = "https://food2fork.com/api/get?key=4b7287373e433cb6ed1c7b5781a42ed2&rId=";
	var mess = req.params['recipe'];
	var id_id = id_url + mess;
	console.log(id_id)
	// console.log(req.params['recipe']);
	// res.render('recipe', { mess: mess});
	request(id_id, function(error, response,body){
		if(!error && response.statusCode == 200){
			var data = JSON.parse(body);
			var id = data.recipe;
			console.log(id);
			res.render("recipe", {
				id: id
			});
		}
	});
});



// request('http://food2fork.com/api/search?key=58e6b619305b0123e254c54789f4f55e&q=shredded%20chicken', function(error, response, body){
// 	console.log('error:', error); // Print the error if one occurred
// 	console.log('statusCode:', response && response.statusCode); //
// 	var body = JSON.parse(body);
//  	console.log('body:', body['recipes'][0]['publisher']); // Print the HTML for the Google homepage.
// });

app.get('*', function(req,res){
	res.render('error')
})




app.listen(port , function(){
	console.log('server is ready');
})
